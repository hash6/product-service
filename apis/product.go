package apis

import (
	"log"
	"net/http"
	"strconv"

	"gitlab.com/hash6/product-service/models"

	"github.com/gin-gonic/gin"
	"gitlab.com/hash6/product-service/dao"
	"gitlab.com/hash6/product-service/service"
)

func List(c *gin.Context) {
	s := service.NewProductService(dao.NewProductDAO(), dao.NewUserDAO())

	xUserID, _ := strconv.ParseUint(c.Request.Header.Get("X-USER-ID"), 10, 64)
	xUserIDUInt := uint(xUserID)

	var userID *uint
	if xUserID > 0 {
		userID = &xUserIDUInt
	} else {
		userID = nil
	}
	if products, err := s.List(userID); err != nil {
		c.AbortWithStatus(http.StatusNotFound)
		log.Fatalln(err)
	} else if products == nil {
		c.JSON(http.StatusOK, []models.Product{})
	} else {
		c.JSON(http.StatusOK, products)
	}
}
