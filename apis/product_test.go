package apis

import (
	"net/http"
	"testing"

	"gitlab.com/hash6/product-service/test_data"
)

func TestProduct(t *testing.T) {
	path := test_data.GetTestCaseFolder()
	runAPITests(t, []apiTestCase{
		{"t1 - get a List", "GET", "/products", "/products", "", List, http.StatusOK, path + "/product_t1.json"},
		{"t2 - get a List not found", "GET", "/products/1", "", "", List, http.StatusNotFound, ""},
	})
}
