package mocks

import (
	"context"
	"log"
	"net"

	"gitlab.com/hash6/protos/protogen/go/servicev1"

	"github.com/dariubs/percent"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
)

const bufSize = 1024 * 1024

var lis *bufconn.Listener

// server is used to implement
type server struct{}

func (s *server) DiscountProduct(ctx context.Context, request *servicev1.DiscountProductRequest) (*servicev1.DiscountProductResponse, error) {
	prod := request.Product
	user := request.User
	if user.FirstName == "Ben" {
		prod.PriceInCents = prod.PriceInCents - uint64(percent.Percent(10, int(prod.PriceInCents)))
	}
	return &servicev1.DiscountProductResponse{
		Product: prod,
		Discount: &servicev1.Discount{
			Percentage:   10,
			ValueInCents: prod.PriceInCents,
		},
	}, nil
}

func init() {
	lis = bufconn.Listen(bufSize)
	s := grpc.NewServer()
	servicev1.RegisterDiscountProductServer(s, &server{})
	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Server exited with error: %v", err)
		}
	}()
}

func BufDialer(context.Context, string) (net.Conn, error) {
	return lis.Dial()
}
