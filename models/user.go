package models

import "time"

type User struct {
	Model
	FirstName   string    `gorm:"column:first_name" json:"first_name"`
	LastName    string    `gorm:"column:last_name" json:"last_name"`
	DateOfBirth time.Time `gorm:"column:date_of_birth" json:"date_of_birth"`
}
