package models

type Product struct {
	Model
	Title        string `gorm:"column:title" json:"title"`
	Description  string `gorm:"column:description" json:"description"`
	PriceInCents uint   `gorm:"column:price_in_cents" json:"price_in_cents"`
}
