package main

import (
	"fmt"
	"log"

	"google.golang.org/grpc"

	"gitlab.com/hash6/product-service/apis"
	"gitlab.com/hash6/product-service/cmd"
	"gitlab.com/hash6/product-service/models"
	"gorm.io/driver/postgres"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func main() {
	// load application configurations
	if err := cmd.LoadConfig("./config"); err != nil {
		panic(fmt.Errorf("invalid application configuration: %s", err))
	}

	var err error
	cmd.Config.DiscountClient, err = grpc.Dial(cmd.Config.DiscountAddress, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer cmd.Config.DiscountClient.Close()
	log.Println("Successfully created client")

	// Creates a router without any middleware by default
	r := gin.New()

	// Global middleware
	// Logger middleware will write the logs to gin.DefaultWriter even if you set with GIN_MODE=release.
	// By default gin.DefaultWriter = os.Stdout
	r.Use(gin.Logger())

	// Recovery middleware recovers from any panics and writes a 500 if there was one.
	r.Use(gin.Recovery())

	v1 := r.Group("/api/v1")
	{
		v1.GET("/products", apis.List)
	}

	cmd.Config.DB, cmd.Config.DBErr = gorm.Open(postgres.Open(cmd.Config.DSN), &gorm.Config{})
	if cmd.Config.DBErr != nil {
		panic(cmd.Config.DBErr)
	}

	cmd.Config.DB.AutoMigrate(&models.Product{}) // This is needed for generation of schema for postgres image.
	cmd.Config.DB.AutoMigrate(&models.User{})    // This is needed for generation of schema for postgres image.

	log.Println("Successfully connected to database")

	r.Run(fmt.Sprintf(":%v", cmd.Config.ServerPort))

}
