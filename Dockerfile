FROM golang:1.15-alpine AS build

# Build outside of GOPATH, it's simpler when using Go Modules.
WORKDIR /src

# Copy dependencies and solve them first to take advantage of layer caching.
COPY vendor /vendor

# Copy everything else.
COPY . .

# Build a static binary.
RUN CGO_ENABLED=0 GOOS=linux go build -mod vendor -a -installsuffix cgo -o . .

# Verify if the binary is truly static.
#RUN ldd /src/product-service 2>&1 | grep -q 'Not a valid dynamic program'

FROM alpine
COPY --from=build /src/product-service /product-service
COPY --from=build /src/config /config

#ENV WAIT_VERSION 2.7.2
#ADD https://github.com/ufoscout/docker-compose-wait/releases/download/$WAIT_VERSION/wait /wait
#RUN chmod +x /wait

ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.5.0/wait /wait
RUN chmod +x /wait


EXPOSE 8080
CMD /wait && /product-service

