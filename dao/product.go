package dao

import (
	"gitlab.com/hash6/product-service/cmd"
	"gitlab.com/hash6/product-service/models"
)

type ProductDAO struct{}

func NewProductDAO() *ProductDAO {
	return &ProductDAO{}
}

func (dao *ProductDAO) List() ([]models.Product, error) {
	var listProducts []models.Product

	err := cmd.Config.DB.Find(&listProducts).Error

	return listProducts, err
}
