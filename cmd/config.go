package cmd

import (
	"fmt"

	"google.golang.org/grpc"

	"gorm.io/gorm"

	"github.com/spf13/viper"
)

// Config is global object that holds all application level variables.
var Config appConfig

type appConfig struct {
	DiscountClient *grpc.ClientConn
	// the shared DB ORM object
	DB *gorm.DB
	// the error thrown be GORM when using DB ORM object
	DBErr error
	// the server port. Defaults to 8080
	ServerPort int `mapstructure:"server_port"`
	// the data source name (DSN) for connecting to the database. required.
	DSN             string `mapstructure:"dsn"`
	DiscountAddress string `mapstructure:"discount_service_address"`
}

// LoadConfig loads cmd from files
func LoadConfig(configPaths ...string) error {
	v := viper.New()
	v.SetConfigName("default")
	v.SetEnvPrefix("product-service")
	v.SetConfigType("yaml")
	v.AutomaticEnv()

	Config.DSN = v.GetString("DSN")
	Config.DiscountAddress = v.GetString("discount_service_address")
	v.SetDefault("server_port", 8888)

	for _, path := range configPaths {
		v.AddConfigPath(path)
	}
	if err := v.ReadInConfig(); err != nil {
		return fmt.Errorf("failed to read the configuration file: %s", err)
	}
	return v.Unmarshal(&Config)
}
