module gitlab.com/hash6/product-service

go 1.15

require (
	github.com/dariubs/percent v0.0.0-20200128140941-b7801cf1c7e2
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/protobuf v1.4.3
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.5.1
	gitlab.com/hash6/protos v0.0.0-20201213175834-de10971a7840
	google.golang.org/grpc v1.34.0
	gorm.io/driver/postgres v1.0.5
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.20.8
)
