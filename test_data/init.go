package test_data

import (
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/hash6/product-service/cmd"
	"gitlab.com/hash6/product-service/models"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func init() {
	err := cmd.LoadConfig("../config")
	if err != nil {
		panic(err)
	}
	cmd.Config.DB, cmd.Config.DBErr = gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	cmd.Config.DB.Exec("PRAGMA foreign_keys = ON") // SQLite defaults to `foreign_keys = off'`
	if cmd.Config.DBErr != nil {
		panic(cmd.Config.DBErr)
	}
	cmd.Config.DB.AutoMigrate(&models.User{})
	cmd.Config.DB.AutoMigrate(&models.Product{})
}

func ResetDB() *gorm.DB {
	if err := cmd.Config.DB.Migrator().DropTable(&models.User{}); err != nil {
		fmt.Println(err)
	}

	if err := cmd.Config.DB.Migrator().DropTable(&models.Product{}); err != nil {
		fmt.Println(err)
	}

	cmd.Config.DB.AutoMigrate(&models.User{})
	cmd.Config.DB.AutoMigrate(&models.Product{})
	if err := runSQLFile(cmd.Config.DB, getSQLFile()); err != nil {
		panic(fmt.Errorf("error while initializing test database: %s", err))
	}
	return cmd.Config.DB
}

func getSQLFile() string {
	return "../test_data/db.sql"
}

func GetTestCaseFolder() string {
	return "../test_data/test_case_data"
}

// Executes SQL file specified by file argument
func runSQLFile(db *gorm.DB, file string) error {
	s, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}
	lines := strings.Split(string(s), ";")
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		if result := db.Exec(line); result.Error != nil {
			fmt.Println(line)
			return result.Error
		}
	}
	return nil
}
