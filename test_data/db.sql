INSERT INTO "products" ("id", "created_at", "updated_at", "title", "description", "price_in_cents") VALUES
(1, '2020-12-14 03:15:21.302637+00', '2020-12-14 03:15:49.456697+00', 'iphone', 'celular', 100);
INSERT INTO "products" ("id", "created_at", "updated_at", "title", "description", "price_in_cents") VALUES
(2, '2020-12-14 03:15:21.302637+00', '2020-12-14 03:15:49.456697+00', 'samsung', 'celular', 1000);
INSERT INTO "users" ("id", "created_at", "updated_at", "first_name", "last_name", "date_of_birth") VALUES
(1, '2020-12-14 03:16:16.577388+00', '1990-12-14 03:16:16.577388+00',  'jose', 'martins', '2020-12-14 12:00:00+00');
INSERT INTO "users" ("id", "created_at", "updated_at", "first_name", "last_name", "date_of_birth") VALUES
(2, '2020-12-14 03:16:16.577388+00', '2020-12-14 03:16:16.577388+00', 'rafael', 'martins', '1991-1-23 12:00:00+00');
