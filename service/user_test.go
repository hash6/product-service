package service

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hash6/product-service/models"
)

func TestNewUserService(t *testing.T) {
	dao := newMockUserDAO()
	s := NewUserService(dao)
	assert.Equal(t, dao, s.dao)
}

func TestUserService_Get(t *testing.T) {
	s := NewUserService(newMockUserDAO())
	user, err := s.Get(2)

	dateOfBirth, _ := time.Parse("2006-01-02", "2000-12-31")

	if assert.Nil(t, err) && assert.NotNil(t, user) {
		assert.Equal(t, "Ben", user.FirstName)
		assert.Equal(t, "Doe", user.LastName)
		assert.Equal(t, dateOfBirth, user.DateOfBirth)
	}

	user, err = s.Get(100)
	assert.NotNil(t, err)
}

func newMockUserDAO() userDAO {
	dateOfBirth, _ := time.Parse("2006-01-02", "1999-12-31")
	dateOfBirthSecond, _ := time.Parse("2006-01-02", "2000-12-31")
	return &mockUserDAO{
		records: []models.User{
			{Model: models.Model{ID: 1}, FirstName: "John", LastName: "Smith", DateOfBirth: dateOfBirth},
			{Model: models.Model{ID: 2}, FirstName: "Ben", LastName: "Doe", DateOfBirth: dateOfBirthSecond},
		},
	}
}

func (m *mockUserDAO) Get(id uint) (*models.User, error) {
	for _, record := range m.records {
		if record.ID == id {
			return &record, nil
		}
	}
	return nil, errors.New("not found")
}

type mockUserDAO struct {
	records []models.User
}
