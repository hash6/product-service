package service

import "gitlab.com/hash6/product-service/models"

type userDAO interface {
	Get(id uint) (*models.User, error)
}

type UserService struct {
	dao userDAO
}

func NewUserService(dao userDAO) *UserService {
	return &UserService{dao}
}

func (s *UserService) Get(id uint) (*models.User, error) {
	return s.dao.Get(id) // No additional logic, just return the query result
}
