package service

import (
	"context"
	"testing"

	"google.golang.org/grpc"

	"gitlab.com/hash6/product-service/mocks"

	"gitlab.com/hash6/product-service/cmd"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hash6/product-service/models"
)

func TestNewProductService(t *testing.T) {
	prodDao := newMockProductDAO()
	userDao := newMockUserDAO()
	s := NewProductService(prodDao, userDao)
	assert.Equal(t, prodDao, s.prodDao)
	assert.Equal(t, userDao, s.userDao)
}

func TestUserService_List(t *testing.T) {
	s := NewProductService(newMockProductDAO(), newMockUserDAO())

	ctx := context.Background()

	// Mock dicsount client
	cmd.Config.DiscountClient, _ = grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(mocks.BufDialer), grpc.WithInsecure())
	defer cmd.Config.DiscountClient.Close()

	userID := uint(2)
	products, _ := s.List(&userID)
	prods := newProductsListDiscount()
	for index, prod := range products {
		assert.Equal(t, prod.PriceInCents, prods[index].PriceInCents)
		assert.Equal(t, prod.Description, prods[index].Description)
		assert.Equal(t, prod.Title, prods[index].Title)
	}

	userID = uint(1)
	products, _ = s.List(&userID)
	prods = newProductsListWithoutDiscount()
	for index, prod := range products {
		assert.Equal(t, prod.PriceInCents, prods[index].PriceInCents)
		assert.Equal(t, prod.Description, prods[index].Description)
		assert.Equal(t, prod.Title, prods[index].Title)
	}

	cmd.Config.DiscountClient.Close()

	products, _ = s.List(nil)
	prods = newProductsListWithoutDiscount()
	for index, prod := range products {
		assert.Equal(t, prod.PriceInCents, prods[index].PriceInCents)
		assert.Equal(t, prod.Description, prods[index].Description)
		assert.Equal(t, prod.Title, prods[index].Title)
	}

}

func newMockProductDAO() productDAO {
	return &mockProductDAO{
		records: newProductsListWithoutDiscount(),
	}
}

// Mock List function that replaces real product DAO
func (m *mockProductDAO) List() ([]models.Product, error) {
	return m.records, nil
}

type mockProductDAO struct {
	records []models.Product
}

func newProductsListWithoutDiscount() []models.Product {
	return []models.Product{
		{models.Model{ID: 1}, "iphone 12", "celular", 100},
		{models.Model{ID: 2}, "samsung s21", "celular", 1000},
	}
}

func newProductsListDiscount() []models.Product {
	return []models.Product{
		{models.Model{ID: 1}, "iphone 12", "celular", 90},
		{models.Model{ID: 2}, "samsung s21", "celular", 900},
	}
}
