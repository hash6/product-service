package service

import (
	"context"
	"fmt"
	"log"

	"github.com/golang/protobuf/ptypes"
	"gitlab.com/hash6/product-service/cmd"
	"gitlab.com/hash6/protos/protogen/go/servicev1"

	"gitlab.com/hash6/product-service/models"
)

type productDAO interface {
	List() ([]models.Product, error)
}

type ProductService struct {
	prodDao productDAO
	userDao userDAO
}

func NewProductService(prodDao productDAO, userDao userDAO) *ProductService {
	return &ProductService{prodDao, userDao}
}

func (s *ProductService) List(userId *uint) ([]models.Product, error) {
	var userGRPC servicev1.User
	if userId != nil {
		user, err := s.userDao.Get(*userId)
		if err != nil {
			log.Println(err)
		}
		userGRPC = userModelToUserGRPC(*user)
	}

	products, err := s.prodDao.List()
	if err != nil {
		log.Println(err)
	}

	var productsNew []models.Product
	for _, prod := range products {
		fmt.Printf("")
		client := servicev1.NewDiscountProductClient(cmd.Config.DiscountClient)

		prodGRPC := productModelToProductGRPC(prod)
		request := &servicev1.DiscountProductRequest{
			User:    &userGRPC,
			Product: &prodGRPC,
		}

		resp, err := client.DiscountProduct(context.Background(), request)
		if err != nil {
			log.Println("Service discount error.", err)
			productsNew = append(productsNew, prod)
		} else {
			fmt.Printf("Receive response => [%v]", resp.String())
			prod.PriceInCents = uint(resp.Discount.ValueInCents)
			productsNew = append(productsNew, prod)
		}
	}

	return productsNew, nil
}

func userModelToUserGRPC(user models.User) servicev1.User {
	dateOfBirth, _ := ptypes.TimestampProto(user.DateOfBirth)
	return servicev1.User{
		FirstName:   user.FirstName,
		LastName:    user.LastName,
		DateOfBirth: dateOfBirth,
	}
}

func productModelToProductGRPC(product models.Product) servicev1.Product {
	return servicev1.Product{
		PriceInCents: uint64(product.PriceInCents),
		Title:        product.Title,
		Description:  product.Description,
	}
}
